import React from "react";

import Link from "../common/Link/Link";
import {FooterNavigation} from "../../utils/links";
import FacebookIcon from "../../public/img/facebook.svg";
import InstagramIcon from "../../public/img/instagram.svg";
import LinkedinIcon from "../../public/img/linkedin.svg";
import {INFORMATION} from "../../mockData/generalMockdata";

import styles from "./Footer.module.scss";

const Footer = () => (
  <footer className={styles["footer"]}>
    <div className={styles["footer__content"]}>
      <div className={styles["footer__main"]}>
        <div className={styles["footer__main-left"]}>
          <div className={styles["footer__links-wrapper"]}>
            <div className={styles["footer__links-wrapper-title"]}>
              Menu
            </div>
            {FooterNavigation.general.map((link) => (
              <Link
                key={link.title}
                className={styles["footer__main-link"]}
                href={link.href}
              >
                {link.title}
              </Link>
            ))}
          </div>
          <div className={styles["footer__links-wrapper"]}>
            <div className={styles["footer__links-wrapper-title"]}>
              Services
            </div>
            {FooterNavigation.services.map(link => (
              <Link
                key={link.title}
                className={styles["footer__main-link"]}
                href={link.href}
              >
                {link.title}
              </Link>
            ))}
          </div>
        </div>
        <div className={styles["footer__main-right"]}>
          <div className={styles["footer__follow-us-text"]}>
            Follow us
          </div>
          <div className={styles["footer__social-media-icons"]}>
            <Link href="/" className={styles["footer__social-media-icon-wrapper"]}>
              <FacebookIcon className={styles["footer__social-media-icon"]} />
            </Link>
            <Link href="/" className={styles["footer__social-media-icon-wrapper"]}>
              <InstagramIcon className={styles["footer__social-media-icon"]} />
            </Link>
            <Link href="/" className={styles["footer__social-media-icon-wrapper"]}>
              <LinkedinIcon className={styles["footer__social-media-icon"]} />
            </Link>
          </div>
          <div className={styles["footer__contacts-link-wrapper"]}>
            <Link
              className={styles["footer__phone-number"]}
              href={`tel:${INFORMATION.PHONE_NUMBER}`}
            >
              {INFORMATION.PHONE_NUMBER}
            </Link>
            <span>
                {INFORMATION.EMAIL}
              </span>
          </div>
        </div>
      </div>
      <div className={styles["footer__bottom"]}>
        <div className={styles["footer__bottom-copyright"]}>© 2021 matuojam.lt</div>
      </div>
    </div>
  </footer>
);

export default Footer;
