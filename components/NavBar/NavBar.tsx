import React, {useEffect, useState} from "react";

import MainNav from "./MainNav/MainNav";
import BurgerNavigation from "./BurgerNavigation/BurgerNavigation";
import Logo from "../common/Logo/Logo";
import {joinTruthy} from "../../utils/utils";
import PageNavigationButton from "../common/PageNavigationButton/PageNavigationButton";
import {LINKS} from "../../utils/links";

import styles from "./NavBar.module.scss";

interface INavBar {
  isDark?: boolean
}

const NavBar = ({ isDark }: INavBar) => {
  const [burgerNavigationOpen, setBurgerNavigationOpen] = useState(false);
  const [isScrolling, setIsScrolling] = useState(false);

  const handleBurgerNavigationToggle = () => {
    setBurgerNavigationOpen(!burgerNavigationOpen);
  };

  const recalculateNavbarState = () => {
    if (window.scrollY === 0) {
      setIsScrolling(false);
    }
    if (window.scrollY > 0) {
      setIsScrolling(true);
    }
  }

  useEffect(() => {
    recalculateNavbarState();

    const handleScroll = () => {
      recalculateNavbarState();
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const isNavbarDark = isScrolling || burgerNavigationOpen || isDark;

  return (
    <nav
      className={joinTruthy([
        styles["navbar"],
        isNavbarDark && styles["navbar--dark"]
      ])}
    >
      <div className={styles["navbar__container"]}>
        <div className={styles["navbar__left"]}>
          <Logo
            className={styles["navbar__logo"]}
            isDark={isNavbarDark}
          />
        </div>
        <div
          className={joinTruthy([
            styles["navbar__burger-navigation"],
            burgerNavigationOpen && styles["navbar__burger-navigation--open"]
          ])}
        >
          <BurgerNavigation />
        </div>
        <div className={styles["navbar__right"]}>
          <div
            className={styles["navbar__toggle-burger"]}
            onClick={handleBurgerNavigationToggle}
          >
          <span
            className={joinTruthy([
              styles["navbar__toggle-burger-icon"],
              burgerNavigationOpen && styles["navbar__toggle-burger-icon--close"]
            ])}
          />
          </div>
          <MainNav isDark={isNavbarDark} />
          <PageNavigationButton
            href={LINKS.userLogin.href}
            classNames={{
              wrapper: styles["navbar__action-button-wrapper"],
              link: styles["navbar__action-button-link"],
              button: styles["navbar__action-button"]
            }}
          >
            Login
          </PageNavigationButton>
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
