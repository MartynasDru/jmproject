import React from "react";

import Link from "../../common/Link/Link";
import {NavBarNavigation} from "../../../utils/links";
import {joinTruthy} from "../../../utils/utils";

import styles from "./MainNav.module.scss";

interface IMainNav {
  isDark?: boolean
}

const MainNav = ({ isDark }: IMainNav) => (
  <div className={joinTruthy([
    styles["main-nav"],
    isDark && styles["main-nav--scrolling"]
  ])}>
    {
      NavBarNavigation.map(linkData => (
        <div
          key={linkData.title}
          className={styles["main-nav__link-wrapper"]}
        >
          {
            linkData.href ? (
              <Link
                href={linkData.href}
                className={styles["main-nav__link"]}
              >
                {linkData.title}
              </Link>
            ) : (
              <>
                <div className={joinTruthy([
                  styles["main-nav__link"],
                  styles["main-nav__link--with-dropdown"]
                ])}>
                  {linkData.title}
                </div>
                {linkData.subPages && (
                  <div className={styles["main-nav__submenu"]}>
                    <div  className={styles["main-nav__submenu-content-wrapper"]}>
                      {linkData.subPages.map(subPage => (
                        <Link
                          key={subPage.title}
                          className={styles["main-nav__submenu-link"]}
                          href={subPage.href}
                        >
                          {subPage.title}
                        </Link>
                      ))}
                    </div>
                  </div>
                )}
              </>
            )
          }
        </div>
      ))
    }
  </div>
);

export default MainNav;
