import * as React from 'react';

import BurgerNavigationLink from "./BurgerNavigationLink/BurgerNavigationLink";
import PageNavigationButton from "../../common/PageNavigationButton/PageNavigationButton";
import {LINKS, NavBarNavigation} from "../../../utils/links";
import LockIcon from "../../../public/img/lock.svg";

import styles from './BurgerNavigation.module.scss';

const BurgerNavigation = () => (
  <ul className={styles["burger-navigation"]}>
    {NavBarNavigation.map(linkData => (
      <BurgerNavigationLink
        key={linkData.title}
        linkData={linkData}
      />
    ))}
    <li className={styles["burger-navigation__link"]}>
      <PageNavigationButton
        href={LINKS.userLogin.href}
        classNames={{
          link: styles["burger-navigation__action-button-link"],
          button: styles["burger-navigation__action-button"]
        }}
      >
        <LockIcon
          className={styles["burger-navigation__action-button-icon"]}
        />
        Login
      </PageNavigationButton>
    </li>
  </ul>
);

export default BurgerNavigation;