import React, {useState} from "react";

import Link from "../../../common/Link/Link";
import ToggleIcon from "../../../../public/img/next.svg";
import {joinTruthy} from "../../../../utils/utils";
import {INavigationLinkData} from "../../../../utils/links";

import styles from './BurgerNavigationLink.module.scss';

interface IBurgerNavigationLink {
  linkData: INavigationLinkData
}

const BurgerNavigationLink = ({ linkData }: IBurgerNavigationLink) => {
  const [isExpanded, setIsExpanded] = useState(false);

  const handleSubPagesToggle = () => {
    setIsExpanded(!isExpanded);
  }

  return (
    <li className={styles["burger-navigation-link"]}>
      {
        linkData.subPages ? (
          <>
            <div
              className={styles["burger-navigation-link__expand-link"]}
              onClick={handleSubPagesToggle}
            >
              {linkData.title}
              <ToggleIcon
                className={joinTruthy([
                  styles["burger-navigation-link__expand-icon"],
                  isExpanded && styles["burger-navigation-link__expand-icon--expanded"]
                ])}
              />
            </div>
            <div
              className={joinTruthy([
                styles["burger-navigation-link__subpages-container"],
                isExpanded && styles["burger-navigation-link__subpages-container--expanded"]
              ])}
            >
              {linkData.subPages.map(subPage => (
                <Link
                  className={styles["burger-navigation-link__subpage-link"]}
                  key={subPage.title}
                  href={subPage.href}
                >
                  {subPage.title}
                </Link>
              ))}
            </div>
          </>
        ) : (
          <Link href={linkData.href}>
            {linkData.title}
          </Link>
        )
      }
    </li>
  );
};

export default BurgerNavigationLink;