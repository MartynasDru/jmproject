import React from "react";

import Image from "next/image";

import styles from "./Slide.module.scss";

interface ISlide {
  review: string,
  date: string,
  name: string,
  service: string,
  city: string
}

const Slide = ({ review, date, name, service, city }: ISlide) => (
  <div className={styles["slide"]}>
    <div className={styles["slide__content-wrapper"]}>
      <div className={styles["slide__review-rating"]}>
        <Image
          src="/img/l6-review-star.png"
          height={25}
          width={158}
        />
        <div className={styles["slide__review-date"]}>{date}</div>
      </div>
      <div className={styles["slide__review-text"]}>
        {review}
      </div>
      <div className={styles["slide__reviewer-information-wrapper"]}>
        <div className={styles["slide__reviewer-name"]}>
          {name}
        </div>
        <div className={styles["slide__reviewer-service"]}>
          {service}
        </div>
        <div className={styles["slide__reviewer-city"]}>
          {city}
        </div>
      </div>
    </div>
  </div>
);

export default Slide;
