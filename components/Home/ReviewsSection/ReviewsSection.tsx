import React from "react";
import Slider from "react-slick";

import Slide from "./Slide/Slide";
import {CAROUSEL_SETTINGS, USER_REVIEWS} from "./utils/constants";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import styles from "./ReviewsSection.module.scss";

const CarouselStatistics = () => (
  <section className={styles["reviews-section"]}>
    <div className={styles["reviews-section__content"]}>
      <h2 className={styles["reviews-section__title"]}>
        Lorem ipsum dolor sit amet
      </h2>
      <div className={styles["reviews-section__title-subtext"]}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      </div>
      <Slider
        {...CAROUSEL_SETTINGS}
        className={styles["reviews-section__carousel"]}
        dotsClass={styles["reviews-section__carousel-dots"]}
      >
        {USER_REVIEWS.map((review) => (
          <Slide
            key={review.review}
            name={review.name}
            service={review.service}
            city={review.city}
            date={review.date}
            review={review.review}
          />
        ))}
      </Slider>
    </div>
    <div className={styles["reviews-section__divider"]} />
  </section>
);

export default CarouselStatistics;
