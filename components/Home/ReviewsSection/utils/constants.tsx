interface IUserReviews {
  name: string,
  service: string,
  city: string,
  date: string,
  review: string,
}

export const USER_REVIEWS: Array<IUserReviews> = [
  {
    name: "Example name",
    service: "Example service",
    city: "Example city",
    date: "Example Date",
    review: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  },
  {
    name: "Example name",
    service: "Example service",
    city: "Example city",
    date: "Example Date",
    review: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  },
  {
    name: "Example name",
    service: "Example service",
    city: "Example city",
    date: "Example Date",
    review: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  }
];

export const CAROUSEL_SETTINGS = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 2,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 5000,
  centerMode: false,
  arrows: false,
  responsive: [
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 1,
      },
    }
  ],
};
