import React from "react";

import OffersRequestForm from "./OffersRequestForm/OffersRequestForm";
import RoutesIcon from "../../../public/img/route.svg";
import EuroIcon from "../../../public/img/euro.svg";
import {joinTruthy} from "../../../utils/utils";
import styles from "./Hero.module.scss";

const Hero = () => (
  <section className={styles["hero"]}>
    <div
      className={styles["hero__container"]}
    >
      <div
        className={styles["hero__text-wrapper"]}
      >
        <h1 className={styles["hero__title"]}>Lorem ipsum dolor sit amet.</h1>
        <div className={styles["hero__text"]}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem. Aliquam tempus tellus pellentesque arcu.
        </div>
        <div className={styles["hero__benefit"]}>
          <RoutesIcon className={styles["hero__benefit-icon"]} />
          Lorem ipsum
        </div>
        <div className={styles["hero__benefit"]}>
          <EuroIcon
            className={joinTruthy([
              styles["hero__benefit-icon"],
              styles["hero__benefit-icon--euro"]
            ])}
          />
          Lorem ipsum
        </div>
      </div>
      <div
        className={styles["hero__form-wrapper"]}
      >
        <OffersRequestForm />
      </div>
    </div>
    <div className={styles["hero__divider"]} />
    <div className={styles["hero__blob-wrapper"]}>
      <div className={styles["hero__blob"]} />
    </div>
  </section>
);

export default Hero;
