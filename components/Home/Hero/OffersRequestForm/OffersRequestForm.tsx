import React, {useRef} from "react";
import {Form, Formik} from "formik";

import FormSelect from "../../../common/FormComponents/FormSelect/FormSelect";
import FormInput from "../../../common/FormComponents/FormInput/FormInput";
import Link from "../../../common/Link/Link";
import Button from "../../../common/Button/Button";
import {AREA_TYPES_OPTIONS, ServicesMockData, SERVICES_OPTIONS} from "../../../../mockData/servicesMockData";

import styles from "./OffersRequestForm.module.scss";

const OffersRequestForm = () => {
  const formRef = useRef(null);

  const handleGetOffersSubmit = (values, actions) => {
    // Placeholder for future form submit handling
    setTimeout(() => {
      actions.setSubmitting(false);
    }, 1000);
  }

  return (
    <div className={styles["offers-request-form"]}>
      <Formik
        initialValues={{
          service: "",
          areaType: "",
          areaSpace: "",
          cadastralNumber: "",
        }}
        onSubmit={handleGetOffersSubmit}
      >
        {({ values, setFieldValue, isSubmitting }) => (
          <Form
            ref={formRef}
            className={styles["offers-request-form__form"]}
          >
            <FormSelect
              name="service"
              label="Service"
              placeholder="Select a service"
              options={SERVICES_OPTIONS}
              classNames={{
                container: styles["offers-request-form__input-container"],
                inputField: styles["offers-request-form__input-field"]
              }}
              setFieldValue={setFieldValue}
              value={values.service}
              isRequiredField
            />
            {values.service === ServicesMockData.service1.value && (
              <FormSelect
                name="areaType"
                label="Area type"
                placeholder="Select area type"
                options={AREA_TYPES_OPTIONS}
                classNames={{
                  container: styles["offers-request-form__input-container"],
                  inputField: styles["offers-request-form__input-field"]
                }}
                setFieldValue={setFieldValue}
                value={values.areaType}
                isRequiredField
              />
            )}
            <FormInput
              name="areaSpace"
              label="Area space"
              placeholder="Enter area space"
              classNames={{
                container: styles["offers-request-form__input-container"],
                inputField: styles["offers-request-form__input-field"]
              }}
              setFieldValue={setFieldValue}
              value={values.areaSpace}
              type="number"
              step={0.01}
              min={0}
              isRequiredField
            />
            <FormInput
              name="cadastralNumber"
              placeholder="XXXX/YYYY:ZZZZ"
              label="Cadastral nr."
              classNames={{
                container: styles["offers-request-form__input-container"],
                inputField: styles["offers-request-form__input-field"]
              }}
              setFieldValue={setFieldValue}
              value={values.cadastralNumber}
              isRequiredField
            />
            <div className={styles["offers-request-form__privacy-agreement"]}>
              <i>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit&nbsp;
                <Link href="/" className={styles["offers-request-form__privacy-agreement-link"]}>Nullam venenatis</Link>.
              </i>
            </div>
            <Button
              className={styles["offers-request-form__action-button"]}
              isDisabled={isSubmitting}
            >
              Submit
            </Button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default OffersRequestForm;