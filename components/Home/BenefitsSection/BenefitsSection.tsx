import React from "react";
import Image from "next/image";

import {joinTruthy} from "../../../utils/utils";

import styles from "./BenefitsSection.module.scss";

interface IBenefitsSection {
  classNames?: { [key: string]: string },
  isReverse?: boolean,
  subImageSrc: string,
  imageSrc: string,
  title?: string,
  text?: string,
}

const BenefitsSection = ({ classNames, isReverse, subImageSrc, imageSrc, title, text }: IBenefitsSection) => (
  <section
    className={joinTruthy([
      styles["benefits-section"],
      classNames?.section
    ])}
  >
    <div
      className={joinTruthy([
        styles["benefits-section__content-wrapper"],
        isReverse && styles["benefits-section__content-wrapper--reverse"]
      ])}
    >
      <div className={styles["benefits-section__images-column"]}>
        <div className={styles["benefits-section__images-wrapper"]}>
          <div
            className={classNames?.subImageWrapper}
          >
            <img src={subImageSrc} alt="background-image-detail" />
          </div>
          <div>
            <Image
              src={imageSrc}
              width="410"
              height="842"
              alt="matuojam.lt logo"
            />
          </div>
        </div>
      </div>
      <div className={styles["benefits-section__text-column"]}>
        <div className={styles["benefits-section__text-wrapper"]}>
          <h2
            className={styles["benefits-section__title"]}
          >
            {title || "Lorem ipsum"}
          </h2>
          <p
            className={styles["benefits-section__text"]}
          >
            {text || "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem."}
          </p>
        </div>
      </div>
    </div>
  </section>
);

export default BenefitsSection;