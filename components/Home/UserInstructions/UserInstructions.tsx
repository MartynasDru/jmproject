import React from "react";
import Image from "next/image";

import {joinTruthy} from "../../../utils/utils";
import FormIcon from "../../../public/img/contact-form.svg";

import styles from "./UserInstructions.module.scss";

const UserInstructions = () => (
  <section className={styles["benefits-section"]}>
    <div className={styles["benefits-section__content-wrapper"]}>
      <h2 className={styles["benefits-section__title"]}>
        Lorem ipsum
      </h2>
      <div
        className={styles["benefits-section__content"]}
      >
        <div>
          <div
            className={styles["benefits-section__arrow-1-wrapper"]}
          >
            <Image
              src="/img/arrow-1.png"
              height={89}
              width={224}
            />
          </div>
          <div
            className={styles["benefits-section__arrow-2-wrapper"]}
          >
            <Image
              src="/img/arrow-2.png"
              height={72}
              width={228}
            />
          </div>
        </div>
        <div
          className={joinTruthy([
            styles["benefits-section__benefit-column"],
            styles["benefits-section__benefit-column--first"]
          ])}
        >
          <div className={styles["benefits-section__benefit-icon-wrapper"]}>
            <FormIcon className={styles["benefits-section__benefit-icon"]} />
          </div>
          <h3 className={styles["benefits-section__benefit-title"]}>
            Lorem ipsum
          </h3>
          <p className={styles["benefits-section__benefit-text"]}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem.
          </p>
        </div>
        <div
          className={joinTruthy([
            styles["benefits-section__benefit-column"],
            styles["benefits-section__benefit-column--second"]
          ])}
        >
          <div className={styles["benefits-section__benefit-icon-wrapper"]}>
            <Image
              src="/img/search.png"
              height={36}
              width={36}
            />
          </div>
          <h3 className={styles["benefits-section__benefit-title"]}>
            Lorem ipsum
          </h3>
          <p className={styles["benefits-section__benefit-text"]}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem.
          </p>
        </div>
        <div
          className={joinTruthy([
            styles["benefits-section__benefit-column"],
            styles["benefits-section__benefit-column--third"]
          ])}
        >
          <div className={styles["benefits-section__benefit-icon-wrapper"]}>
            <Image
              src="/img/right-layer.png"
              height={36}
              width={46}
            />
          </div>
          <h3 className={styles["benefits-section__benefit-title"]}>
            Lorem ipsum
          </h3>
          <p className={styles["benefits-section__benefit-text"]}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem.
          </p>
        </div>
      </div>
    </div>
  </section>
);

export default UserInstructions;