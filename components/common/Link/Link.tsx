import React from "react";
import NextLink from "next/link";

import {joinTruthy} from "../../../utils/utils";
import {LINKS} from "../../../utils/links";

import styles from "./Link.module.scss";

interface ILink {
    children: React.ReactNode,
    href: string,
    className?: string,
    onClick?: (e: React.MouseEvent<HTMLAnchorElement>) => void,
}

const Link = ({ children, href, className, onClick }: ILink) => (
  <NextLink
    href={href || LINKS.home.href}
  >
    <a
      className={joinTruthy([
        styles["link"],
        className
      ])}
      onClick={onClick}
    >
      {children}
    </a>
  </NextLink>
);

export default Link;