import React from 'react';

import {joinTruthy} from "../../../utils/utils";

import styles from "./Button.module.scss";

interface IButton {
    children?: React.ReactNode,
    className?: string,
    type?: "button" | "submit" | "reset",
    onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void,
    isDisabled?: boolean,
}

const Button = ({ children, className, type, onClick, isDisabled }: IButton) => (
  <button
    type={type || "submit"}
    className={joinTruthy([
      styles["button"],
      className
    ])}
    onClick={onClick}
    disabled={isDisabled}
  >
    {children}
  </button>
);

export default Button;