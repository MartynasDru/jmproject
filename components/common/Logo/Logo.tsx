import React from "react";

import Link from "../Link/Link";
import {joinTruthy} from "../../../utils/utils";
import {LINKS} from "../../../utils/links";

import styles from "./Logo.module.scss";

interface ILogo {
    className?: string,
    isDark?: boolean
}

const Logo = ({ className, isDark }: ILogo) => (
  <Link
    href={LINKS.home.href}
    className={joinTruthy([
      styles["logo"],
      className
    ])}
  >
    <img
      src={`/img/matuojam-logo-${isDark ? "dark" : "light"}.png`}
      alt="matuojam.lt logo"
    />
  </Link>
);

export default Logo;