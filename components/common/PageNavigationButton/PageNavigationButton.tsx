import React from "react";

import {joinTruthy} from "../../../utils/utils";
import Link from "../Link/Link";
import Button from "../Button/Button";

import styles from "./PageNavigationButton.module.scss";

interface IClassNames {
    wrapper?: string,
    link?: string,
    button?: string
}

interface IPageNavigationButton {
    children?: React.ReactNode,
    classNames?: IClassNames,
    href: string,
}

const PageNavigationButton = ({ children, classNames, href }: IPageNavigationButton) => (
  <div
    className={joinTruthy([
      styles["page-navigation-button"],
      classNames?.wrapper
    ])}
  >
    <Link
      href={href}
      className={joinTruthy([
        styles["page-navigation-button__link"],
        classNames?.link
      ])}
    >
      <Button
        className={joinTruthy([
          styles["page-navigation-button__button"],
          classNames?.button
        ])}
      >
        {children}
      </Button>
    </Link>
  </div>
);

export default PageNavigationButton;