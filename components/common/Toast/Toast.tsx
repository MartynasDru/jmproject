import React from "react";
import {ToastContainer} from "react-toastify";

const Toast = () => (
  <ToastContainer
    position="top-center"
    closeButton={false}
    autoClose={false}
    rtl={false}
    draggable={false}
    closeOnClick={false}
  />
);

export default Toast;