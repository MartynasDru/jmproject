import React from "react";
import {default as ReactSelect} from "react-select";

const reactSelectStyles = {
  container: styles => ({
    ...styles,
    fontSize: "1.6rem"
  }),
  control: styles => ({
    ...styles,
    borderColor: "rgba(0, 0, 0, .2) !important",
    borderRadius: ".4rem",
    boxShadow: styles.borderColor === "#2684FF" ? ".2rem 0 .5rem rgb(81, 203, 238)" : "",
    outline: styles.borderColor === "#2684FF" ? "-webkit-focus-ring-color auto 1px !important" : "",
    transition: "none",
  }),
  valueContainer: styles => ({
    ...styles,
    fontWeight: 600,
    padding: "1.1rem 2.5rem 1.1rem 1.5rem",
    width: "calc(100% - 3.6rem)"
  }),
  placeholder: styles => ({
    ...styles,
    color: "rgba(0, 0, 0, .3)",
    fontWeight: 600,
    margin: 0
  }),
  input: styles => ({
    ...styles,
    margin: 0,
    lineHeight: 1.5,
    padding: 0,
    "input": {
      width: "3px"
    }
  }),
  indicatorsContainer: styles => ({
    ...styles,
    "> div": {
      padding: "0 1.5rem",
    },
    "svg": {
      transition: ".15s",
      "&:hover": {
        fill: "rgb(29, 41, 63)",
      }
    },
  }),
  indicatorSeparator: styles => ({
    ...styles,
    display: "none"
  })
};

interface IOption {
  value: string,
  [key: string]: any,
}

interface ISelect {
  placeholder?: string,
  options?: Array<IOption>,
  onChange: () => void,
  inputValue?: string,
}

const Select = ({ placeholder, options, onChange, inputValue }: ISelect) => (
  <ReactSelect
    inputId="input-id"
    placeholder={placeholder}
    options={options}
    styles={reactSelectStyles}
    onChange={onChange}
    value={options.find(option => option.value === inputValue) || ""}
  />
);

export default Select;