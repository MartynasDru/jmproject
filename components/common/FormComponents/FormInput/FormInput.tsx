import React, {useEffect, useRef} from "react";
import {Field} from "formik";

import {joinTruthy} from "../../../../utils/utils";
import ClearInputIcon from "../../../../public/img/x-icon.svg";

import styles from "./FormInput.module.scss";

interface IClassNames {
  container?: string,
  inputField?: string
}

interface IFormInput {
  children?: React.ReactNode,
  classNames?: IClassNames,
  setFieldValue,
  isRequiredField?: boolean,
  isClearDisabled?: boolean,
  fieldRequiredValidationMessage?: string,
  leadingIcon?: React.ReactNode,
  type?: string,
  component?: React.ReactNode,
  min?: number,
  step?: number,
  name?: string,
  value: string,
  label?: string,
  placeholder?: string,
}

const FormInput = ({
  children,
  classNames,
  setFieldValue,
  isRequiredField,
  isClearDisabled,
  fieldRequiredValidationMessage,
  leadingIcon,
  type,
  component,
  min,
  step,
  name,
  value,
  label,
  placeholder,
}: IFormInput) => {
  const inputFieldRef = useRef(null);

  const handleInputReset = () => {
    setFieldValue?.(name, "", false);
  }

  useEffect(() => {
    const inputField = inputFieldRef.current;

    if (inputField) {
      if (type === "email") {
        if (inputField.validity.typeMismatch) {
          inputField.setCustomValidity("Invalid email address.");
        } else {
          if (isRequiredField) {
            if (inputField.validity.valueMissing) {
              const validationMessage = fieldRequiredValidationMessage || "Required Field.";
              inputField.setCustomValidity(validationMessage);
            } else {
              inputField.setCustomValidity("");
            }
          } else {
            inputField.setCustomValidity("");
          }
        }
      } else {
        if (isRequiredField) {
          if (inputField.validity.valueMissing) {
            const validationMessage = fieldRequiredValidationMessage || "Required Field.";
            inputField.setCustomValidity(validationMessage);
          } else {
            inputField.setCustomValidity("");
          }
        }
      }
    }
  }, [value])

  const formattedPlaceholder = `${placeholder || ""}${isRequiredField ? "*" : ""}`;

  return (
    <>
      {label && (
        <div className={styles["form-input__label"]}>
          {label}
        </div>
      )}
      <div
        className={joinTruthy([
          styles["form-input"],
          classNames?.container,
        ])}
      >
        {leadingIcon && (
          <div
            className={joinTruthy([
              styles["form-input__icon"],
              styles["form-input__icon--leading-icon"]
            ])}
          >
            {leadingIcon}
          </div>
        )}
        <Field
          component={component || "input"}
          innerRef={inputFieldRef}
          className={joinTruthy([
            styles["form-input__input-field"],
            type === "number" && styles["form-input__input-field--clear-not-visible"],
            classNames?.inputField
          ])}
          type={type}
          name={name}
          value={value}
          placeholder={formattedPlaceholder}
          required={isRequiredField}
          step={step}
          min={min}
        >
          {children}
        </Field>
        {!isClearDisabled && !!value && type !== "number" && (
          <ClearInputIcon
            className={joinTruthy([
              styles["form-input__icon"],
              styles["form-input__icon--clear-icon"]
            ])}
            onClick={handleInputReset}
          />
        )}
      </div>
    </>
  );
};

export default FormInput;