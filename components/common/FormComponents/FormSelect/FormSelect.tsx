import React, {useEffect, useRef} from "react";
import {Field} from "formik";

import {joinTruthy} from "../../../../utils/utils";
import Select from "../../Select/Select";

import styles from "./FormSelect.module.scss";

interface IClassNames {
  container?: string,
  inputField?: string
}

interface IOption {
  value: string,
  [key: string]: any,
}

interface IFormSelect {
  classNames?: IClassNames,
  value: string,
  isRequiredField?: boolean,
  fieldRequiredValidationMessage?: string,
  placeholder?: string,
  setFieldValue: (name: string, value: string) => void,
  name: string,
  options: Array<IOption>,
  leadingIcon?: React.ReactNode,
  label?: string
}

const FormSelect = ({
  classNames,
  value,
  isRequiredField,
  fieldRequiredValidationMessage,
  setFieldValue,
  placeholder,
  name,
  options,
  leadingIcon,
  label,
}: IFormSelect) => {
  const inputFieldRef = useRef(null);

  useEffect(() => {
    const inputField = inputFieldRef.current;

    if (inputField) {
      if (isRequiredField) {
        if (!value) {
          const validationMessage = fieldRequiredValidationMessage || "Required field.";
          inputField.setCustomValidity(validationMessage);
        } else {
          inputField.setCustomValidity("");
        }
      }
    }
  }, [value]);

  const handleOnChange = (option) => {
    setFieldValue(name, option.value);
  };

  const formattedPlaceholder = `${placeholder}${isRequiredField ? "*" : ""}`;

  return (
    <>
      {label && (
        <div className={styles["form-select__label"]}>
          {label}
        </div>
      )}
      <div className={joinTruthy([
        styles["form-select"],
        classNames?.container,
      ])}>
        {leadingIcon && (
          <div
            className={joinTruthy([
              styles["form-select__icon"],
              styles["form-select__icon--leading-icon"]
            ])}
          >
            {leadingIcon}
          </div>
        )}
        <Field
          name={name}
          options={options}
          component={Select}
          value={value}
          placeholder={formattedPlaceholder}
          required={isRequiredField}
          onChange={handleOnChange}
          inputValue={value}
        />
        <input
          ref={inputFieldRef}
          tabIndex={-1}
          className={styles["form-select__hidden-input"]}
          autoComplete="off"
          value={value}
          required={isRequiredField}
          onChange={() => {}}
        />
      </div>
    </>
  );
};

export default FormSelect;