import React from "react";
import Head from "next/head";

import NavBar from "../NavBar/NavBar";
import Footer from "../Footer/Footer";
import Toast from "../common/Toast/Toast";

import "react-toastify/dist/ReactToastify.css";
import styles from "./Layout.module.scss";

interface ILayout {
    children: React.ReactNode,
    title?: string,
    metaDescription?: string,
    isNavbarHidden?: boolean,
    isNavbarDark?: boolean,
    isFooterHidden?: boolean,
}

const Layout = ({
  children,
  title,
  metaDescription,
  isNavbarHidden,
  isNavbarDark,
  isFooterHidden,
} : ILayout) => (
  <div>
    <Head>
      <title>{title || "Matuojam.lt"}</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      {metaDescription && (
        <meta
          name="description"
          content={metaDescription}
        />
      )}
    </Head>
    <div
      className={styles["layout"]}
    >
      <Toast />
      {!isNavbarHidden && <NavBar isDark={isNavbarDark} />}
      <div className={styles["layout__content"]}>
        {children}
      </div>
      {!isFooterHidden && <Footer/>}
    </div>
  </div>
);

export default Layout;