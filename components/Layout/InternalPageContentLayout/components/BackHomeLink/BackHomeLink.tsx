import React from "react";

import ArrowIcon from "../../../../../public/img/arrow.svg";
import Link from "../../../../common/Link/Link";
import {LINKS} from "../../../../../utils/links";

import styles from "./BackHomeLink.module.scss";

const BackHomeLink = () => {
  return (
    <div className={styles["back-home-link"]}>
      <Link
        className={styles["back-home-link__link"]}
        href={LINKS.home.href}
      >
        <ArrowIcon className={styles["back-home-link__icon"]} />
        Home
      </Link>
    </div>
  );
};

export default BackHomeLink;