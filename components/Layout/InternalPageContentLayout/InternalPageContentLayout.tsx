import React from "react";

import BackHomeLink from "./components/BackHomeLink/BackHomeLink";

import styles from "./InternalPageContentLayout.module.scss";

interface IInternalPageContentLayout {
  children: React.ReactNode,
  title?: string,
  isBottomBackHomeLinkVisible?: boolean,
}

const InternalPageContentLayout = ({ children, title, isBottomBackHomeLinkVisible }: IInternalPageContentLayout) => (
  <div className={styles["internal-page"]}>
    <div className={styles["internal-page__content"]}>
      <h1 className={styles["internal-page__title"]}>
        {title || "Lorem ipsum"}
      </h1>
      <div className={styles["internal-page__main"]}>
        {children}
        {isBottomBackHomeLinkVisible && <BackHomeLink />}
      </div>
    </div>
  </div>
);

export default InternalPageContentLayout;