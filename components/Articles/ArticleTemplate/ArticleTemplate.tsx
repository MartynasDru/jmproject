import React from "react";

import {IArticle} from "../../../mockData/articlesMockData";

import styles from "./ArticleTemplate.module.scss";

interface IArticleTemplate {
  articleData: Array<IArticle>
}

const ArticleTemplate = ({ articleData }: IArticleTemplate) => (
  <div className={styles["article"]}>
    {articleData.map((paragraph) => (
      <div
        key={paragraph.title}
        className={styles["article__paragraph"]}
      >
        <div className={styles["article__paragraph-title-wrapper"]}>
          {paragraph.icon}
          <h1 className={styles["article__paragraph-title"]}>
            {paragraph.title}
          </h1>
        </div>
        {paragraph.description && (
          <p className={styles["article__paragraph-text"]}>
            {paragraph.description}
          </p>
        )}
        {paragraph.list && (
          <ul className={styles["article__list"]}>
            {paragraph.list.map((listInfo) => (
              <li key={listInfo}>{listInfo}</li>
            ))}
          </ul>
        )}
      </div>
    ))}
  </div>
);

export default ArticleTemplate;