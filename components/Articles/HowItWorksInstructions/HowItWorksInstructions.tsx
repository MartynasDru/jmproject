import React from "react";

import InstructionsStep from "./InstructionsStep/InstructionsStep";
import {INSTRUCTIONS_STEPS} from "./utils/constants";

import styles from "./HowItWorksInstructions.module.scss";

const HowItWorksInstructions = () => (
  <ul className={styles["how-it-works-instructions"]}>
    {INSTRUCTIONS_STEPS.map((instructionStep, index) => (
      <InstructionsStep
        key={instructionStep.title}
        title={instructionStep.title}
        number={index + 1}
        text={instructionStep.text}
        isRight={index % 2 > 0}
      />
    ))}
  </ul>
);

export default HowItWorksInstructions;