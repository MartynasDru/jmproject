import React from "react";

import {joinTruthy} from "../../../../utils/utils";

import styles from "./InstructionsStep.module.scss";

interface IInstructionsStep {
  isRight: boolean,
  title: string,
  text: string,
  number: number,
}

const InstructionsStep = ({ isRight, title, text, number }: IInstructionsStep) => (
  <li className={styles["instructions-step"]}>
    <div
      className={joinTruthy([
        styles["instructions-step__text"],
        isRight && styles["instructions-step__text--right"],
      ])}
    >
      <h4 className={styles["instructions-step__title"]}>{title}</h4>
      <p className={styles["instructions-step__paragraph"]}>
        {text}
      </p>
    </div>
    <div className={styles["instructions-step__number"]}>
      {number}
    </div>
  </li>
);

export default InstructionsStep;