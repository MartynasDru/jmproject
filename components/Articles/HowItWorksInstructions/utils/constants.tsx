import React from "react";

interface IInstructionStep {
  title: string,
  text: string
}

export const INSTRUCTIONS_STEPS: Array<IInstructionStep> = [
  {
    title: "Lorem ipsum 1",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem.",
  },
  {
    title: "Lorem ipsum 2",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem.",
  },
  {
    title: "Lorem ipsum 3",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem.",
  },
  {
    title: "Lorem ipsum 4",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem.",
  },
  {
    title: "Lorem ipsum 5",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem.",
  },
];