import React from "react";
import {Form, Formik} from "formik";

import FormInput from "../../common/FormComponents/FormInput/FormInput";
import Button from "../../common/Button/Button";
import {joinTruthy} from "../../../utils/utils";

import styles from "./ContactForm.module.scss";

const ContactForm = () => {

  const handleGetOffersSubmit = (values, actions) => {
    setTimeout(() => {
      actions.resetForm();
    }, 2000);
  }

  return (
    <div className={styles["contact-form"]}>
      <h2 className={styles["contact-form__title"]}>
        Lorem ipsum
      </h2>
      <div className={styles["contact-form__title-subtext"]}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem.
      </div>
      <Formik
        initialValues={{
          name: "",
          email: "",
          phone: "",
          message: ""
        }}
        onSubmit={handleGetOffersSubmit}
      >
        {({ values, setFieldValue, isSubmitting }) => (
          <Form className={styles["contact-form__form"]}>
            <div className={styles["contact-form__inputs-wrapper"]}>
              <div className={styles["contact-form__contacts-inputs-wrapper"]}>
                <FormInput
                  name="name"
                  classNames={{
                    container: styles["contact-form__input-container"],
                    inputField: styles["contact-form__input"]
                  }}
                  placeholder="Name"
                  value={values.name}
                  setFieldValue={setFieldValue}
                  isRequiredField
                />
                <FormInput
                  type="email"
                  name="email"
                  classNames={{
                    container: styles["contact-form__input-container"],
                    inputField: styles["contact-form__input"]
                  }}
                  placeholder="Email"
                  value={values.email}
                  setFieldValue={setFieldValue}
                  isRequiredField
                />
                <FormInput
                  name="phone"
                  classNames={{
                    container: styles["contact-form__input-container"],
                    inputField: styles["contact-form__input"]
                  }}
                  placeholder="Phone"
                  value={values.phone}
                  setFieldValue={setFieldValue}
                />
              </div>
              <div className={styles["contact-form__message-wrapper"]}>
                <FormInput
                  name="message"
                  classNames={{
                    container: joinTruthy([
                      styles["contact-form__input-container"],
                      styles["contact-form__input-container--textarea"]
                    ]),
                    inputField: joinTruthy([
                      styles["contact-form__input"],
                      styles["contact-form__input--textarea"]
                    ]),
                  }}
                  placeholder="Message"
                  component="textarea"
                  value={values.message}
                  setFieldValue={setFieldValue}
                  isClearDisabled
                  isRequiredField
                />
              </div>
            </div>
            <Button
              className={styles["contact-form__form-submit-button"]}
              isDisabled={isSubmitting}
            >
              Send
            </Button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default ContactForm;