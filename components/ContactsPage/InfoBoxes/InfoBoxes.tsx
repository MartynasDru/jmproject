import React from "react";

import AddressIcon from "../../../public/img/address-icon.svg";
import PhoneIcon from "../../../public/img/phone.svg";
import EmailIcon from "../../../public/img/email.svg";
import {joinTruthy} from "../../../utils/utils";
import Link from "../../common/Link/Link";
import {INFORMATION} from "../../../mockData/generalMockdata";

import styles from "./InfoBoxes.module.scss";

const InfoBoxes = () => (
  <div className={styles["info-boxes"]}>
    <div className={styles["info-boxes__boxes-wrapper"]}>
      <div className={styles["info-boxes__box"]}>
        <AddressIcon
          className={styles["info-boxes__box-icon"]}
        />
        <div className={styles["info-boxes__box-text"]}>
          {INFORMATION.ADDRESS}
        </div>
      </div>
      <div className={styles["info-boxes__box"]}>
        <PhoneIcon
          className={joinTruthy([
            styles["info-boxes__box-icon"],
            styles["info-boxes__box-icon--phone"]
          ])}
        />
        <div className={styles["info-boxes__box-text"]}>
          <Link
            href={`tel:${INFORMATION.PHONE_NUMBER}`}
          >
            {INFORMATION.PHONE_NUMBER}
          </Link>
        </div>
      </div>
      <div className={styles["info-boxes__box"]}>
        <EmailIcon
          className={styles["info-boxes__box-icon"]}
        />
        <div className={styles["info-boxes__box-text"]}>
          {INFORMATION.EMAIL}
        </div>
      </div>
    </div>
    <div className={styles["info-boxes__other-details-boxes-wrapper"]}>
      <div className={styles["info-boxes__other-details-box"]}>
        <h3 className={styles["info-boxes__other-details-box-title"]}>
          Lorem ipsum
        </h3>
        <ul className={styles["info-boxes__details-list"]}>
          <li className={styles["info-boxes__details-list-row"]}>
            {INFORMATION.COMPANY_NAME}
          </li>
          <li className={styles["info-boxes__details-list-row"]}>
            {INFORMATION.COMPANY_CODE}
          </li>
          <li className={styles["info-boxes__details-list-row"]}>
            {INFORMATION.PVM_CODE}
          </li>
        </ul>
      </div>
      <div  className={styles["info-boxes__other-details-box"]}>
        <h3 className={styles["info-boxes__other-details-box-title"]}>
          Lorem ipsum
        </h3>
        <ul className={styles["info-boxes__details-list"]}>
          <li className={styles["info-boxes__details-list-row"]}>
            {INFORMATION.BANK.NAME} {INFORMATION.BANK.SWIFT}
          </li>
          <li className={styles["info-boxes__details-list-row"]}>
            {INFORMATION.BANK.ACCOUNT_NUMBER}
          </li>
        </ul>
      </div>
    </div>
  </div>
);

export default InfoBoxes;