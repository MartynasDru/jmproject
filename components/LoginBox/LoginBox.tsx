import React, {useState} from "react";
import {Form, Formik} from "formik";

import Link from "../common/Link/Link";
import FormInput from "../common/FormComponents/FormInput/FormInput";
import Button from "../common/Button/Button";
import {LINKS} from "../../utils/links";

import styles from "./LoginBox.module.scss";

const LoginBox = () => {
  const [errorText, setErrorText] = useState("");


  const handleLoginSubmit = (values, actions) => {
    // Placeholder for future authentication
    setTimeout(() => {
      setErrorText("Login failed");
      actions.setSubmitting(false);
    }, 1000);
  }

  return (
    <div
      className={styles["login-box"]}
    >
      <div className={styles["login-box__header"]}>
        <h1 className={styles["login-box__title"]}>User login</h1>
      </div>
      <Formik
        initialValues={{
          email: "",
          password: ""
        }}
        onSubmit={handleLoginSubmit}
      >
        {({ values, setFieldValue, isSubmitting }) => (
          <Form className={styles["login-box__form"]}>
            <FormInput
              name="email"
              type="email"
              classNames={{
                container: styles["login-box__input-container"]
              }}
              placeholder="Email"
              value={values.email}
              setFieldValue={setFieldValue}
              isRequiredField
            />
            <FormInput
              name="password"
              type="password"
              classNames={{
                container: styles["login-box__input-container"]
              }}
              placeholder="Password"
              value={values.password}
              setFieldValue={setFieldValue}
              isRequiredField
            />
            {errorText && (
              <div className={styles["login-box__error-box"]}>
                {errorText}
              </div>
            )}
            <Button
              className={styles["login-box__login-button"]}
              isDisabled={isSubmitting}
            >
              Login
            </Button>
          </Form>
        )}
      </Formik>
      <div className={styles["login-box__navigate-back"]}>
        <Link href={LINKS.home.href}>
          Home
        </Link>
      </div>
    </div>
  );
};

export default LoginBox;