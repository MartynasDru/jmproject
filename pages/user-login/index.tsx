import React from "react";

import LoginBox from "../../components/LoginBox/LoginBox";
import Layout from "../../components/Layout/Layout";
import Logo from "../../components/common/Logo/Logo";

import styles from "./styles.module.scss";

const UserLogin = () => (
  <Layout isNavbarHidden isFooterHidden>
    <div className={styles["login-screen"]}>
      <div>
        <Logo
          className={styles["login-screen__logo"]}
          isDark
        />
      </div>
      <div className={styles["login-screen__content"]}>
        <LoginBox />
      </div>
    </div>
  </Layout>
);

export default UserLogin;