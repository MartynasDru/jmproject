import React from "react";

import Layout from "../../components/Layout/Layout";
import InternalPageContentLayout from "../../components/Layout/InternalPageContentLayout/InternalPageContentLayout";
import HowItWorksInstructions from "../../components/Articles/HowItWorksInstructions/HowItWorksInstructions";

const HowItWorks = () => (
  <Layout isNavbarDark>
    <InternalPageContentLayout isBottomBackHomeLinkVisible>
      <HowItWorksInstructions />
    </InternalPageContentLayout>
  </Layout>
);

export default HowItWorks;