import React from "react";

import Layout from "../components/Layout/Layout";
import Hero from "../components/Home/Hero/Hero";
import BenefitsSection from "../components/Home/BenefitsSection/BenefitsSection";
import ReviewsSection from "../components/Home/ReviewsSection/ReviewsSection";
import UserInstructions from "../components/Home/UserInstructions/UserInstructions";
import {joinTruthy} from "../utils/utils";
import {BENEFITS} from "../mockData/benefitsMockData";

import styles from "./app.module.scss";
import homeStyles from "./Home.module.scss";

const Home = () => (
  <Layout>
    <div className={styles["home"]}>
      <Hero/>
      <UserInstructions/>
      {BENEFITS.map((benefit, index) => (
        <BenefitsSection
          key={benefit.type}
          classNames={{
            subImageWrapper: joinTruthy([
              homeStyles["home__sub-image-wrapper"],
              homeStyles[`home__sub-image-wrapper--${benefit.type}`]
            ])
          }}
          isReverse={!!(index % 2)}
          {...benefit}
        />
      ))}
      <ReviewsSection />
    </div>
  </Layout>
);

export default Home;