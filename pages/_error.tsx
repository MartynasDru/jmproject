import React from "react";

import Layout from "../components/Layout/Layout";
import Button from "../components/common/Button/Button";
import Link from "../components/common/Link/Link";

import styles from "./error.module.scss";

export const Error = () => (
  <Layout
    title="Puslapis nerastas - Matuojam.lt"
    isNavbarDark
  >
    <div className={styles["error"]}>
      <h1 className={styles["error__title"]}>
        <div className={styles["error__title-main"]}>Puslapis, kurį įvedėte nerastas.</div>
        <div>Spauskite mygtuką žemiau ir ieškokite pradiniame puslapyje.</div>
      </h1>
      <Button className={styles["error__action-button"]}>
        <Link
          className={styles["error__action-button-link"]}
          href="/"
        >
          Grįžti į pradinį
        </Link>
      </Button>
    </div>
  </Layout>
);

export default Error;