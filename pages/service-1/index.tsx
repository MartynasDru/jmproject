import React from "react";

import Layout from "../../components/Layout/Layout";
import InternalPageContentLayout from "../../components/Layout/InternalPageContentLayout/InternalPageContentLayout";
import ArticleTemplate from "../../components/Articles/ArticleTemplate/ArticleTemplate";
import {SERVICE_1_ARTICLES} from "../../mockData/articlesMockData";

const Service1 = () => (
  <Layout isNavbarDark>
    <InternalPageContentLayout
      isBottomBackHomeLinkVisible
    >
      <ArticleTemplate articleData={SERVICE_1_ARTICLES} />
    </InternalPageContentLayout>
  </Layout>
);

export default Service1;