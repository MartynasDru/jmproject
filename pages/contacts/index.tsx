import React from "react";

import Layout from "../../components/Layout/Layout";
import InternalPageContentLayout from "../../components/Layout/InternalPageContentLayout/InternalPageContentLayout";
import InfoBoxes from "../../components/ContactsPage/InfoBoxes/InfoBoxes";
import ContactForm from "../../components/ContactsPage/ContactForm/ContactForm";

const Contacts = () => (
  <Layout isNavbarDark>
    <InternalPageContentLayout>
      <InfoBoxes />
      <ContactForm />
    </InternalPageContentLayout>
  </Layout>
);

export default Contacts;