import React from "react";
import Document, {Html, Head, Main, NextScript, DocumentContext} from "next/document";

class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html lang="lt">
        <Head>
          <link rel="icon" href="/img/favicon.svg" />
          <link rel="shortcut icon" href="/img/favicon.png" />
          <link rel="apple-touch-startup-image" href="/img/favicon.png" />
          <link rel="apple-touch-icon" href="/img/favicon.png" />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Nunito:400,400i,600,700&display=swap"
          />
          <script
            async
            src="https://www.googletagmanager.com/gtag/js?id=G-9GK0P6R39P"
          />
          <script
            dangerouslySetInnerHTML={{
              __html: `
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
              gtag('config', 'G-9GK0P6R39P', {
                page_path: window.location.pathname,
              });`
            }}
          />
          <meta name="verify-paysera" content="0de78c0aeef86b7281e4d351fac9152b" />
        </Head>
        <body>
          <script data-cfasync="false" type="text/javascript" src="https://app.ecwid.com/script.js?45531185" charSet="utf-8"></script>
          <script type="text/javascript">xProduct()</script>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument