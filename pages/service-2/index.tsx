import React from "react";

import Layout from "../../components/Layout/Layout";
import InternalPageContentLayout from "../../components/Layout/InternalPageContentLayout/InternalPageContentLayout";
import ArticleTemplate from "../../components/Articles/ArticleTemplate/ArticleTemplate";
import {SERVICE_2_ARTICLES} from "../../mockData/articlesMockData";

const Service2 = () => (
  <Layout isNavbarDark>
    <InternalPageContentLayout
      isBottomBackHomeLinkVisible
    >
      <ArticleTemplate articleData={SERVICE_2_ARTICLES} />
    </InternalPageContentLayout>
  </Layout>
);

export default Service2;