interface ILink {
  href: string,
  title: string,
}

interface ILinks {
  [key: string]: ILink
}

export interface INavigationLinkData {
  href?: string,
  title: string,
  subPages?: Array<ILink>
}

interface IFooterNavigationLinks {
  [key: string]: Array<ILink>
}

export const LINKS: ILinks = {
  home: {
    href: "/",
    title: "Home",
  },
  howItWorks: {
    href: "/how-it-works",
    title: "How it works",
  },
  contacts: {
    href: "/contacts",
    title: "Contacts"
  },
  service1: {
    href: "/service-1",
    title: "Service 1"
  },
  service2: {
    href: "/service-2",
    title: "Service 2"
  },
  userLogin: {
    href: "/user-login",
    title: "User Login"
  },
  offerPage: {
    href: "/offer-page",
    title: "Offer page"
  }
}

export const NavBarNavigation: Array<INavigationLinkData> = [
  LINKS.home,
  {
    title: "Services",
    subPages: [
      LINKS.service1,
      LINKS.service2,
    ]
  },
  LINKS.howItWorks,
  LINKS.contacts,
];

export const FooterNavigation: IFooterNavigationLinks = {
  general: [
    LINKS.home,
    LINKS.howItWorks,
    LINKS.contacts,
    LINKS.userLogin
  ],
  services: [
    LINKS.service1,
    LINKS.service2,
  ]
};
