export const joinTruthy = (items: Array<any>, delimiter: string = ' '): string =>
  (items || []).filter(item => !!item).join(delimiter);