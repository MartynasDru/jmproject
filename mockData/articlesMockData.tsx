import React from "react";

import CalendarIcon from "../public/img/calendar.svg";
import MapIcon from "../public/img/map.svg";
import QuestionIcon from "../public/img/question.svg";
import HelpIcon from "../public/img/help.svg";

import styles from "../components/Articles/ArticleTemplate/ArticleTemplate.module.scss";

export interface IArticle {
  title: string,
  icon: JSX.Element,
  description?: string,
  list?: Array<string>
}

export const SERVICE_1_ARTICLES: Array<IArticle> = [
  {
    icon: <MapIcon className={styles["article__paragraph-title-icon"]} />,
    title: "Lorem ipsum dolor sit amet 1?",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem. Aliquam tempus tellus pellentesque arcu.",
    list: [
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
      "Sed consectetur nulla in massa sagittis dignissim.",
      "Nunc id nisl nec libero hendrerit blandit.",
      "Curabitur id nisl vel mauris finibus lobortis.",
    ]
  },
  {
    icon: <CalendarIcon className={styles["article__paragraph-title-icon"]} />,
    title: "Lorem ipsum dolor sit amet 2?",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem. Aliquam tempus tellus pellentesque arcu.",
    list: [
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
      "Sed consectetur nulla in massa sagittis dignissim.",
      "Nunc id nisl nec libero hendrerit blandit.",
      "Curabitur id nisl vel mauris finibus lobortis.",
    ]
  },
  {
    icon: <QuestionIcon className={styles["article__paragraph-title-icon"]} />,
    title: "Lorem ipsum dolor sit amet 3?",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem. Aliquam tempus tellus pellentesque arcu.",
  },
  {
    icon: <HelpIcon className={styles["article__paragraph-title-icon"]} />,
    title: "Lorem ipsum dolor sit amet 4?",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem. Aliquam tempus tellus pellentesque arcu.",
  },
];

export const SERVICE_2_ARTICLES: Array<IArticle> = [
  {
    icon: <MapIcon className={styles["article__paragraph-title-icon"]} />,
    title: "Lorem ipsum dolor sit amet 1?",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem. Aliquam tempus tellus pellentesque arcu.",
    list: [
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
      "Sed consectetur nulla in massa sagittis dignissim.",
      "Nunc id nisl nec libero hendrerit blandit.",
      "Curabitur id nisl vel mauris finibus lobortis.",
    ]
  },
  {
    icon: <CalendarIcon className={styles["article__paragraph-title-icon"]} />,
    title: "Lorem ipsum dolor sit amet 2?",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem. Aliquam tempus tellus pellentesque arcu.",
    list: [
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
      "Sed consectetur nulla in massa sagittis dignissim.",
      "Nunc id nisl nec libero hendrerit blandit.",
      "Curabitur id nisl vel mauris finibus lobortis.",
    ]
  },
  {
    icon: <QuestionIcon className={styles["article__paragraph-title-icon"]} />,
    title: "Lorem ipsum dolor sit amet 3?",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem. Aliquam tempus tellus pellentesque arcu.",
    list: [
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
      "Sed consectetur nulla in massa sagittis dignissim.",
      "Nunc id nisl nec libero hendrerit blandit.",
      "Curabitur id nisl vel mauris finibus lobortis.",
    ]
  },
  {
    icon: <HelpIcon className={styles["article__paragraph-title-icon"]} />,
    title: "Lorem ipsum dolor sit amet 4?",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam venenatis, mi ut tincidunt aliquet, turpis lorem dapibus sem, sit amet ullamcorper turpis lorem in lorem. Aliquam tempus tellus pellentesque arcu.",
    list: [
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
      "Sed consectetur nulla in massa sagittis dignissim.",
      "Nunc id nisl nec libero hendrerit blandit.",
      "Curabitur id nisl vel mauris finibus lobortis.",
    ]
  },
];