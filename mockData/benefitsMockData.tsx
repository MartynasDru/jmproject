export const BENEFITS = [
  {
    type: "benefit-1",
    imageSrc: "/img/mobile-placeholder-img.png",
    subImageSrc: "/img/green-dots.png",
  },
  {
    type: "benefit-2",
    imageSrc: "/img/mobile-placeholder-img.png",
    subImageSrc: "/img/yellow-circle.png",
  },
  {
    type: "benefit-3",
    imageSrc: "/img/mobile-placeholder-img.png",
    subImageSrc: "/img/green-shape.png",
  }
]