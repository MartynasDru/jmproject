interface IBank {
  NAME: string,
  SWIFT: string,
  ACCOUNT_NUMBER: string
}

interface IInformation {
  ADDRESS: string,
  PHONE_NUMBER: string,
  EMAIL: string,
  COMPANY_NAME: string,
  COMPANY_CODE: string,
  PVM_CODE: string,
  BANK: IBank
}

export const INFORMATION: IInformation = {
  ADDRESS: "ADDRESS",
  PHONE_NUMBER: "PHONE_NUMBER",
  EMAIL: "EMAIL",
  COMPANY_NAME: 'COMPANY NAME',
  COMPANY_CODE: "COMPANY_CODE",
  PVM_CODE: "PVM_CODE",
  BANK: {
    NAME: "BANK_NAME",
    SWIFT: "BANK_SWIFT",
    ACCOUNT_NUMBER: "BANK_ACCOUNT_NUMBER"
  }
}