interface IService {
  label: string,
  value: string
}

interface IServices {
  [key: string]: IService
}

interface IAreaType {
  label: string,
  value: string
}

interface IAreaTypes {
  [key: string]: IAreaType
}

export const ServicesMockData: IServices = {
  service1: {
    label: "Service 1",
    value: "Service 1"
  },
  service2: {
    label: "Service 2",
    value: "Service 2"
  }
};

export const AREA_TYPES: IAreaTypes = {
  areaType1: {
    label: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    value: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
  },
  areaType2: {
    label: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    value: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
  },
  areaType3: {
    label: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    value: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
  },
  areaType4: {
    label: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    value: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
  },
}

export const SERVICES_OPTIONS: Array<IService> = [
  ServicesMockData.service1,
  ServicesMockData.service2,
];

export const AREA_TYPES_OPTIONS: Array<IAreaType> = [
  AREA_TYPES.areaType1,
  AREA_TYPES.areaType2,
  AREA_TYPES.areaType3,
  AREA_TYPES.areaType4
];